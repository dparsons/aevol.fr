---
title: "Installation"
date: 2023-09-04T11:42:12+02:00
draft: false
---

Aevol can run on Linux and MacOS X. Windows users can run it using e.g. a Docker container.

## Pre-built packages

Existing pre-built packages are all outdated, we do not recommend to use them. Instead, we recommend installing from source. We apologize for the inconvenience.

## Installation from Source

### Required Dependencies

{{< highlight Shell "linenos=true" >}}
apt install cmake g++ zlib1g-dev
{{< /highlight >}}
or
{{< highlight Shell "linenos=true" >}}
yum install cmake gcc-c++ zlib-devel
{{< /highlight >}}


### Installation Instructions

**TL;DR**:
{{< highlight Shell "linenos=true" >}}
wget https://gitlab.inria.fr/aevol/aevol/-/archive/aevol-9/aevol-aevol-9.tar.gz
tar zxf aevol-aevol-9.tar.gz
cd aevol-aevol-9
mkdir build && cd build
cmake .. -DCMAKE_BUILD_TYPE=Release
make
make install # optional
{{< /highlight >}}

**NB:** To speed up the installation, you might use `make -j X`, where X is the number of threads to attribute to make.
If the installation is interrupted, we recommand deleting the `build` folder, and starting again from line 4.


As for pre-built packages, official releases of aevol are all outdated. We hence recommend you download the latest commit.

You can do that with the following command:
{{< highlight Shell "linenos=true" >}}
wget https://gitlab.inria.fr/aevol/aevol/-/archive/aevol-9/aevol-aevol-9.tar.gz
{{< /highlight >}}

Alternatively you can navigate to [aevol's gitlab page](https://gitlab.inria.fr/aevol/aevol) and click on the "Download" button next to the "Clone" button. Then, select the ".tar.gz" option.

Open a terminal and `cd` into the directory where you've downloaded the source code. Extract the files and `cd` into the extracted directory:
{{< highlight Shell "linenos=true" >}}
tar zxf aevol-aevol-9.tar.gz
cd aevol-aevol-9
{{< /highlight >}}

Create a build dir and `cd` into it:
{{< highlight Shell "linenos=true" >}}
mkdir build && cd build
{{< /highlight >}}

Now you're ready to configure the build. If you want the default build just go for it:
{{< highlight Shell "linenos=true" >}}
cmake .. -DCMAKE_BUILD_TYPE=Release
make
{{< /highlight >}}

If you'd like to compile with non-default options enabled/disabled you can tell cmake what you want before invoking make. For example, to build with statistics of the best individual (which makes simulations take more time but allows you early insight), set without-stats to `false`:
{{< highlight Shell "linenos=true" >}}
cmake .. -DCMAKE_BUILD_TYPE=Release -Dwithout-stats=FALSE
make
{{< /highlight >}}

This will build the aevol libraries and place a bunch of binaries in the `build/bin` directory. There are actually 3 sets of executables that correspond to 3 flavours of aevol. You can identify which flavour a given binary corresponds to through its prefix:
* **aevol_:**		plain old aevol with 2 bases
* **aevol_4b_:**	aevol with 4 bases (ACGT)
* **raevol_:**		aevol with gene regulation networks

Finally, if you have administration privileges, you can make the Aevol programs available to all users on the computer by typing:
{{< highlight Shell "linenos=true" >}}
sudo make install
{{< /highlight >}}
