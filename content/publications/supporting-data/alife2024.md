---
title: "Alife 2024 Supporting Data"
date: 2023-03-31T14:15:19+02:00
draft: false
---

The supporting data for the article "Aevol_4b: Bridging the gap between artificial life and bioinformatics" consists of the sequences of the final 99 sequences and the original tree in both PhyloXML and Newick formats.

[Download the supporting data](https://gitlab.com/aevol/aevol.fr/-/raw/main/supporting-data/alife2024-data.zip)
